package com.aubrun.eric.projet7.springmvc.business.service;

import com.aubrun.eric.projet7.springmvc.consumer.BorrowingConsumer;
import com.aubrun.eric.projet7.springmvc.consumer.ReservationConsumer;
import com.aubrun.eric.projet7.springmvc.model.*;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BorrowingService {

    private final BorrowingConsumer borrowingConsumer;
    private final ReservationConsumer reservationConsumer;
    private final JwtToken jwtToken;

    public BorrowingService(BorrowingConsumer borrowingConsumer, ReservationConsumer reservationConsumer, JwtToken jwtToken) {
        this.borrowingConsumer = borrowingConsumer;
        this.reservationConsumer = reservationConsumer;
        this.jwtToken = jwtToken;
    }

    public ResponseEntity<Borrowings> getAllBorrowing() {
        return borrowingConsumer.getBorrowing(jwtToken);
    }

    public ResponseEntity<Void> addBorrow(Integer bookId) {
        Book book = new Book();
        book.setBookId(bookId);
        return borrowingConsumer.addBorrowing(book, jwtToken);
    }

    public ResponseEntity<Void> extendBorrowing(Integer borrowingId) {
        return borrowingConsumer.extendBorrowing(borrowingId, jwtToken);
    }

    public ResponseEntity<Reservations> getAllReservation() {
        return reservationConsumer.getReservation(jwtToken);
    }

    public ResponseEntity<Void> addReserve(Integer bookId) {
        Book book = new Book();
        book.setBookId(bookId);
        return reservationConsumer.reservation(book, jwtToken);
    }

    public ResponseEntity<Void> deleteReserve(Integer reservationId) {
        reservationConsumer.getReservation(jwtToken);
        return reservationConsumer.deleteReservation(reservationId, jwtToken);
    }

    public ResponseEntity<Void> deleteBorrow(Integer borrowingId) {
        borrowingConsumer.getBorrowing(jwtToken);
        return borrowingConsumer.deleteBorrowing(borrowingId, jwtToken);
    }
}

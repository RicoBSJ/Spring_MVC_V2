package com.aubrun.eric.projet7.springmvc.consumer;

import com.aubrun.eric.projet7.springmvc.model.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ReservationConsumer {

    private final RestTemplate restTemplate;

    public ReservationConsumer(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<Reservations> getReservation(JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<HttpHeaders> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://localhost:8081/biblio-api/borrowings/allReserves/", HttpMethod.GET, entity, Reservations.class);
    }

    public ResponseEntity<Void> reservation(Book book, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Book> entity = new HttpEntity<>(book, headers);
        return restTemplate.exchange("http://localhost:8081/biblio-api/borrowings/reserve/", HttpMethod.POST, entity, Void.class);
    }

    public ResponseEntity<Void> deleteReservation(Integer reservationId, JwtToken jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", jwtToken.getJwt());
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        return restTemplate.exchange("http://localhost:8081/biblio-api/borrowings/" + reservationId, HttpMethod.DELETE, entity, Void.class);
    }
}

<%--
  Created by IntelliJ IDEA.
  User: ricobsj
  Date: 31/01/2021
  Time: 12:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous">
    <title>Liste des emprunts</title>
</head>
<body>
<div class="container">
    <br/>
    <h1>Liste des emprunts de ${sessionScope.userAccount.username}</h1>
    <br/>
    <p>
        <a href="<c:url value="/"/>">Retour à l'accueil</a>
    </p>
    <br/>
    <table class="table">
        <tr>
            <th>Titre</th>
            <th>Nombre d'exemplaire</th>
            <th>Auteur</th>
            <th>Date de fin d'emprunt</th>
            <th>Renouveler</th>
            <th>Restituer</th>
        </tr>
        <c:forEach var="borrowing" items="${borrowings}">
            <tr class="table">
                <td>${borrowing.bookBorrowing.title}</td>
                <td>${borrowing.bookBorrowing.quantity}</td>
                <td>${borrowing.bookBorrowing.bookAuthor.firstName} ${borrowing.bookBorrowing.bookAuthor.lastName}</td>
                <td>${borrowing.endDate}</td>
                <td><c:if test="${!borrowing.renewal}">
                    <form:form modelAttribute="extendBorrowingForm"
                               action="updateBorrowing"
                               method="post">
                        <input type="hidden" id="borrowingId" name="borrowingId" value="${borrowing.borrowingId}"/>
                        <input type="submit" name="tag" value="Renouveler"/>
                    </form:form>
                </c:if>
                    <c:if test="${borrowing.renewal}">
                        <p>Non disponible</p>
                    </c:if>
                </td>
                <td><form:form modelAttribute="deleteBorrowingForm"
                               action="deleteBorrow"
                               method="delete">
                    <input type="hidden" id="borrowingId" name="borrowingId" value="${borrowing.borrowingId}"/>
                    <input type="submit" name="tag" value="Retour"/>
                </form:form>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<br/>
<br/>
<div class="container">
    <br/>
    <h1>Liste des réservations de ${sessionScope.userAccount.username}</h1>
    <br/>
    <table class="table">
        <tr>
            <th>Titre</th>
            <th>Nombre d'exemplaire</th>
            <th>Auteur</th>
            <th>Date de réservation</th>
            <th>Position dans la liste d'attente</th>
            <th>Date retour la plus proche</th>
            <th>Disponibilité</th>
            <th>Annuler</th>
        </tr>
        <c:forEach var="reservation" items="${reservations}">
            <tr class="table">
                <td>${reservation.book.title}</td>
                <td>${reservation.book.quantity}</td>
                <td>${reservation.book.bookAuthor.firstName} ${reservation.book.bookAuthor.lastName}</td>
                <td>${reservation.date}</td>
                <td>${reservation.position}</td>
                <td>${reservation.firstReturnDate}</td>
                <td><c:if test="${reservation.firstReturnDate == null}">
                    <p>Oui</p>
                </c:if>
                    <c:if test="${reservation.firstReturnDate != null}">
                        <p>Non</p>
                    </c:if>
                </td>
                <td><c:if test="${reservation.book.bookId != null}">
                    <form:form modelAttribute="deleteReservationForm"
                               action="delReserve"
                               method="delete">
                        <input type="hidden" id="reservationId" name="reservationId"
                               value="${reservation.reservationId}"/>
                        <input type="submit" name="tag" value="Annuler"/>
                    </form:form>
                </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>
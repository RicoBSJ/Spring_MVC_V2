package com.aubrun.eric.projet7.springmvc.exposition.controller;

import com.aubrun.eric.projet7.springmvc.business.service.BorrowingService;
import com.aubrun.eric.projet7.springmvc.exposition.controller.form.ExtendBorrowingForm;
import com.aubrun.eric.projet7.springmvc.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
public class BorrowingController {

    private final BorrowingService borrowingService;

    public BorrowingController(BorrowingService borrowingService) {
        this.borrowingService = borrowingService;
    }

    @ModelAttribute(value = "reservation")
    public ReservationForm setReservation() {
        return new ReservationForm();
    }

    @ModelAttribute(value = "borrowing")
    public BorrowingDetails setBorrowing() {
        return new BorrowingDetails();
    }

    @GetMapping(value = {"/borrowing", "/getAllBorrowing"})
    public String printAllBorrowings(ModelMap modelMap) {

        Borrowings borrowings = borrowingService.getAllBorrowing().getBody();
        modelMap.addAttribute("borrowings", borrowings);
        Reservations reservations = borrowingService.getAllReservation().getBody();
        modelMap.addAttribute("reservations", reservations);
        return "borrowing";
    }

    @PostMapping("/updateBorrowing")
    public String extendBorrowing(@ModelAttribute("extendBorrowingForm") ExtendBorrowingForm form, ModelMap modelMap) {
        borrowingService.extendBorrowing(form.getBorrowingId());
        Borrowings borrowings = borrowingService.getAllBorrowing().getBody();
        modelMap.addAttribute("borrowings", borrowings);
        Reservations reservations = borrowingService.getAllReservation().getBody();
        modelMap.addAttribute("reservations", reservations);
        return "borrowing";
    }

    @PostMapping(value = "/delReserve")
    public String delReserve(@ModelAttribute("deleteReservationForm") DeleteReservationForm deleteReservationForm, ModelMap modelMap) {
        borrowingService.deleteReserve(deleteReservationForm.getReservationId());
        Borrowings borrowings = borrowingService.getAllBorrowing().getBody();
        modelMap.addAttribute("borrowings", borrowings);
        Reservations reservations = borrowingService.getAllReservation().getBody();
        modelMap.addAttribute("reservations", reservations);
        return "borrowing";
    }

    @PostMapping(value = "/deleteBorrow")
    public String delBorrowing(@ModelAttribute("deleteBorrowingForm") DeleteBorrowingForm deleteBorrowingForm, ModelMap modelMap) {
        borrowingService.deleteBorrow(deleteBorrowingForm.getBorrowingId());
        Borrowings borrowings = borrowingService.getAllBorrowing().getBody();
        modelMap.addAttribute("borrowings", borrowings);
        Reservations reservations = borrowingService.getAllReservation().getBody();
        modelMap.addAttribute("reservations", reservations);
        return "borrowing";
    }
}
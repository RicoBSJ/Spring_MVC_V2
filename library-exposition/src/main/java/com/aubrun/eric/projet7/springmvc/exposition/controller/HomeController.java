package com.aubrun.eric.projet7.springmvc.exposition.controller;

import com.aubrun.eric.projet7.springmvc.business.service.BookService;
import com.aubrun.eric.projet7.springmvc.business.service.BorrowingService;
import com.aubrun.eric.projet7.springmvc.model.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@SessionAttributes("home")
public class HomeController {
    private final BookService bookService;
    private final BorrowingService borrowingService;

    public HomeController(BookService bookService, BorrowingService borrowingService) {
        this.bookService = bookService;
        this.borrowingService = borrowingService;
    }

    @ModelAttribute(value = "reservation")
    public ReservationForm setReservation() {
        return new ReservationForm();
    }

    @ModelAttribute(value = "borrowing")
    public BorrowingForm setBorrowing() {
        return new BorrowingForm();
    }

    @GetMapping(value = {"", "/", "/home", "/homePage"})
    public ModelAndView home() {
        SearchBook searchBook = new SearchBook();
        List<Book> result = bookService.searchBook(searchBook).getBody();
        ModelAndView modelAndView = new ModelAndView("home", "searchBook", new SearchBook());
        modelAndView.addObject("books", result);
        return modelAndView;
    }

    @PostMapping("/borrowing")
    public ModelAndView borrowing(@ModelAttribute("borrowing") BorrowingForm borrowingForm) {
        borrowingService.addBorrow(borrowingForm.getBookId());
        SearchBook searchBook = new SearchBook();
        List<Book> result = bookService.searchBook(searchBook).getBody();
        ModelAndView modelAndView = new ModelAndView("home", "searchBook", new SearchBook());
        modelAndView.addObject("message", "Emprunt réalisé : ");
        modelAndView.addObject("books", result);
        return modelAndView;
    }

    @PostMapping("/reserve")
    public ModelAndView reserve(@ModelAttribute("reservation") ReservationForm reservationForm) {
        borrowingService.addReserve(reservationForm.getBookId());
        SearchBook searchBook = new SearchBook();
        List<Book> result = bookService.searchBook(searchBook).getBody();
        ModelAndView modelAndView = new ModelAndView("home", "searchBook", new SearchBook());
        modelAndView.addObject("message", "Réservation réalisée : ");
        modelAndView.addObject("books", result);
        return modelAndView;
    }
}
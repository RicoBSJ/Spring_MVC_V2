package com.aubrun.eric.projet7.springmvc.model;

public class DeleteBorrowingForm {

    private Integer borrowingId;

    public Integer getBorrowingId() {
        return borrowingId;
    }

    public void setBorrowingId(Integer borrowingId) {
        this.borrowingId = borrowingId;
    }
}

package com.aubrun.eric.projet7.springmvc.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ReservationDetails {

    private Integer reservationId;
    private Book book;
    private UserAccount user;
    private LocalDateTime date;
    private LocalDateTime mailSentDate;
    private Boolean mailSent;
    private Integer position;
    private LocalDate firstReturnDate;

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public LocalDateTime getMailSentDate() {
        return mailSentDate;
    }

    public void setMailSentDate(LocalDateTime mailSentDate) {
        this.mailSentDate = mailSentDate;
    }

    public Boolean getMailSent() {
        return mailSent;
    }

    public void setMailSent(Boolean mailSent) {
        this.mailSent = mailSent;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public LocalDate getFirstReturnDate() {
        return firstReturnDate;
    }

    public void setFirstReturnDate(LocalDate firstReturnDate) {
        this.firstReturnDate = firstReturnDate;
    }
}

package com.aubrun.eric.projet7.springmvc.model;

public class DeleteReservationForm {

    private Integer reservationId;

    public Integer getReservationId() {
        return reservationId;
    }

    public void setReservationId(Integer reservationId) {
        this.reservationId = reservationId;
    }
}

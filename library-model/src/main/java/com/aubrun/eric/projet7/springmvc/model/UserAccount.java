package com.aubrun.eric.projet7.springmvc.model;

import java.util.List;
import java.util.Set;

public class UserAccount {

    private Integer userId;
    private String username;
    private String email;
    private String password;
    private List<BorrowingDetails> borrowingDetailsList;
    private List<ReservationDetails> reservationDetailsList;
    private Set<Role> roleDtos;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<BorrowingDetails> getBorrowingDetailsList() {
        return borrowingDetailsList;
    }

    public void setBorrowingDetailsList(List<BorrowingDetails> borrowingDetailsList) {
        this.borrowingDetailsList = borrowingDetailsList;
    }

    public List<ReservationDetails> getReservationDetailsList() {
        return reservationDetailsList;
    }

    public void setReservationDetailsList(List<ReservationDetails> reservationDetailsList) {
        this.reservationDetailsList = reservationDetailsList;
    }

    public Set<Role> getRoleDtos() {
        return roleDtos;
    }

    public void setRoleDtos(Set<Role> roleDtos) {
        this.roleDtos = roleDtos;
    }
}

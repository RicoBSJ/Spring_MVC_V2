package com.aubrun.eric.projet7.springmvc.model;

public class BorrowingForm {

    private Integer bookId;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }
}
